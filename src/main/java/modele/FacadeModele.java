package modele;

import modele.Exceptions.CaseNonVideException;
import modele.Exceptions.InvalidNameException;

public interface FacadeModele {
    public Joueur getJ1();

    public Joueur getJ2();

    public Plateau getPlateau();

    public Joueur getCurrentPlayer();

    public Joueur getWinner();

    public void setJoueurs(Joueur j1, Joueur j2) throws InvalidNameException;

    public void jouerCoup(int index) throws CaseNonVideException;

    public boolean estTerminee();

    public void ragequit(Joueur rajeux);

    void reinitialiser();
}
