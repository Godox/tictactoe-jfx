package modele;

public class Joueur {

    private final Symbole symbole;
    private String name;

    public Joueur(String name, Symbole symbole) {
        this.name = name;
        this.symbole = symbole;
    }

    public Symbole getSymbole() {
        return symbole;
    }

    public String getName() {
        return name;
    }

}
