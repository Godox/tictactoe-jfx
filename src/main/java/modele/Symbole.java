package modele;

public class Symbole {

    public static Symbole CERCLE = new Symbole('O', 7);
    public static Symbole CROIX = new Symbole('X', 2);
    public static Symbole VIDE = new Symbole(' ', 0);

    private final char character;
    private final int value;

    private Symbole(char c, int i){
     this.character = c;
     this.value = i;
    }

    public char getCharacter() {
        return character;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(this.character);
    }
}
