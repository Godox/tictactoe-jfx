package modele.Exceptions;

public class CaseNonVideException extends Exception {
    public CaseNonVideException(String message) {
        super(message);
    }
}
