package modele;

import java.util.ArrayList;

public class Plateau {

    public ArrayList<Symbole> cases;

    public Plateau() {
        this.cases = new ArrayList<>();
        for (int i = 0; i < 9; i++) {
            this.cases.add(Symbole.VIDE);
        }
    }

}
