package modele;

import modele.Exceptions.CaseNonVideException;
import modele.Exceptions.InvalidNameException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FacadeTicTacToe implements FacadeModele {

    private Joueur j1;
    private Joueur j2;
    private Plateau plateau;
    private Joueur winner;
    private Joueur currentPlayer;

    public FacadeTicTacToe() {
        this.plateau = new Plateau();
        winner = null;
    }

    public void setJoueurs(Joueur j1, Joueur j2) throws InvalidNameException {
        if (j1.getName().equals(j2.getName()))
            throw new InvalidNameException("Le nom des deux joueurs est le même");
        for (Joueur j : new Joueur[]{j1, j2})
            if (j.getName().length() <= 3) {
                throw new InvalidNameException("Le nom " + j.getName() + " est trop court");
            }
        this.j1 = j1;
        this.j2 = j2;
        currentPlayer = this.j1;
    }

    public Joueur getJ1() {
        return j1;
    }

    public Joueur getJ2() {
        return j2;
    }

    public Plateau getPlateau() {
        return plateau;
    }

    private boolean verifLine(List<Symbole> line) {
        int sum = 0;
        for (Symbole s : line) {
            sum += s.getValue();
        }
        return sum == 21 || sum == 6;
    }

    private boolean isFullBoard() {
        for (Symbole symbole : this.plateau.cases) {
            if (symbole.getValue() == 0) {
                return false;
            }
        }
        return true;
    }

    private boolean verifAllLignes() {
        ArrayList<Symbole> board = this.plateau.cases;
        return (
                //Lignes
                verifLine(board.subList(0, 3)) ||
                verifLine(board.subList(3, 6)) ||
                verifLine(board.subList(6, 9)) ||
                //Diagonales
                verifLine(Arrays.asList(board.get(0), board.get(4), board.get(8))) ||
                verifLine(Arrays.asList(board.get(2), board.get(4), board.get(6))) ||
                //Colonnes
                verifLine(Arrays.asList(board.get(0), board.get(3), board.get(5))) ||
                verifLine(Arrays.asList(board.get(1), board.get(4), board.get(6))) ||
                verifLine(Arrays.asList(board.get(2), board.get(5), board.get(8))));
    }

    public boolean estTerminee() {
        if (winner == null) {
            if (verifAllLignes()) {
                this.winner = currentPlayer;
                return true;
            }
            return isFullBoard();
        }
        return true;
    }

    private void switchPlayers() {
        currentPlayer = currentPlayer == this.j1 ? this.j2 : this.j1;
    }

    public Joueur getWinner() {
        return winner;
    }

    public Joueur getCurrentPlayer() {
        return currentPlayer;
    }

    public void jouerCoup(int index) throws CaseNonVideException {
        if (!(this.plateau.cases.get(index) == Symbole.VIDE)) {
            throw new CaseNonVideException("La case ciblée est déjà remplie");
        }
        plateau.cases.set(index, currentPlayer.getSymbole());
        if (!this.estTerminee()) {
            this.switchPlayers();
        }
    }

    public void ragequit(Joueur rajeux) {
        this.winner = rajeux == this.j1 ? this.j2 : this.j1;
    }

    public void reinitialiser() {
        this.plateau = new Plateau();
        this.j1 = null;
        this.j2 = null;
        this.winner = null;
        this.currentPlayer = null;
    }

}
