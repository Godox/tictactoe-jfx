package controleur.ordres;

public interface LanceurOrdre {
    /**
     * Permet d'enregistrer un abonné pour
     * différents types d'événements
     * @param ecouteurOrdre
     * @param types
     */
    void abonnement(EcouteurOrdre ecouteurOrdre,
                    Ordre... types);

    /**
     * Permet de diffuser un événement
     * aux abonnés concernés, (Ne pas utiliser dans les vues)
     * @param ordre
     */
    void fireOrdre(Ordre ordre);

}