package controleur.ordres;


import vues.interfaces.Vue;

import java.util.function.Consumer;

public enum GoToOrdre implements Ordre{

    GOTO_PREPARATION,
    GOTO_PLATEAU;

    private Vue view;

    @Override
    public Type getType() {
        return Type.GOTO;
    }

    public void setGotoView(Vue vue) {
        this.view = vue;
    }

    public Vue getView() {
        return view;
    }
}
