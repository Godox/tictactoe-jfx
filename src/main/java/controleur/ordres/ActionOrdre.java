package controleur.ordres;

public enum ActionOrdre implements Ordre{

    ORDRE_HELLO,
    ORDRE_REFRESH,
    ORDRE_CASE_NON_VIDE, ORDRE_PARTIE_TERMINEE, ORDRE_NOUVEAU_TOUR;

    @Override
    public Type getType() {
        return Type.ACTION;
    }
}
