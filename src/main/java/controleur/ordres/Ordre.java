package controleur.ordres;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public interface Ordre {

    Type getType();

    enum Type {
        // Type des ordres
        GOTO,
        ACTION;

    }

}
