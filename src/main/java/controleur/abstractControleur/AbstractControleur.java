package controleur.abstractControleur;

import controleur.Controleur;
import controleur.ordres.ActionOrdre;
import controleur.ordres.EcouteurOrdre;
import controleur.ordres.GoToOrdre;
import controleur.ordres.Ordre;
import vues.interfaces.GestionnaireVue;

import java.util.*;

public abstract class AbstractControleur implements Controleur {

    private GestionnaireVue abstractGestionnaireVue;
    private Map<Ordre, Collection<EcouteurOrdre>> abonnes;
    public GestionnaireVue getGestionnaireVue() {
        return abstractGestionnaireVue;
    }


    public AbstractControleur(GestionnaireVue gestionnaireVue) {
        this.abstractGestionnaireVue = gestionnaireVue;
        this.abonnes = new HashMap<>();
        List.of(ActionOrdre.values()).forEach(t ->
                this.abonnes.put(t, new ArrayList<>()));
        List.of(GoToOrdre.values()).forEach(t ->
                this.abonnes.put(t, new ArrayList<>()));
    }

    @Override
    public void abonnement(EcouteurOrdre ecouteurOrdre, Ordre... types) {
        Arrays.stream(types).forEach(t ->
                this.abonnes.get(t).add(ecouteurOrdre));
    }

    /**
     * @param ordre
     * @implNote Do not use into Views
     */
    @Override
    public void fireOrdre(Ordre ordre) {
        this.abonnes.get(ordre).forEach(ec ->
                ec.traiter(ordre));
    }

    public void goTo(GoToOrdre ordre){
        this.fireOrdre(ordre);
    }

    public abstract void run();

}
