package controleur;

import controleur.abstractControleur.AbstractControleur;
import controleur.ordres.ActionOrdre;
import javafx.scene.control.Alert;
import modele.Exceptions.CaseNonVideException;
import modele.Exceptions.InvalidNameException;
import modele.FacadeModele;
import modele.Joueur;
import modele.Plateau;
import modele.Symbole;
import vues.interfaces.GestionnaireVue;

import static controleur.ordres.GoToOrdre.GOTO_PLATEAU;
import static controleur.ordres.GoToOrdre.GOTO_PREPARATION;


public class ControleurImpl extends AbstractControleur {

    private FacadeModele facadeModele;

    public ControleurImpl(GestionnaireVue gestionnaireVue, FacadeModele facadeModele, ControleurSetUp controleurSetUp) {
        super(gestionnaireVue);
        this.facadeModele = facadeModele;
        controleurSetUp.setUp(this, getGestionnaireVue());
    }

    @Override
    public void run() {
        this.fireOrdre(GOTO_PREPARATION);
    }

    public void sayHello() {
        this.fireOrdre(ActionOrdre.ORDRE_HELLO);
    }

    public void lancerPartie(String p1, String p2) {
        try {
            this.facadeModele.setJoueurs(new Joueur(p1, Symbole.CERCLE), new Joueur(p2, Symbole.CROIX));
            this.fireOrdre(GOTO_PLATEAU);
            this.fireOrdre(ActionOrdre.ORDRE_NOUVEAU_TOUR);
        } catch (InvalidNameException e) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Erreur dans le nom des joueurs");
            a.setContentText(e.getMessage());
            a.showAndWait();
        }
    }

    public Plateau getPlateau() {
        return this.facadeModele.getPlateau();
    }

    public void verifFinPartie(){
        if (this.facadeModele.estTerminee()){
            this.fireOrdre(ActionOrdre.ORDRE_PARTIE_TERMINEE);
        }else{
            this.fireOrdre(ActionOrdre.ORDRE_NOUVEAU_TOUR);
        }
    }

    public void jouerCoup(int caseIndex) {
        try {
            this.facadeModele.jouerCoup(caseIndex);
            this.fireOrdre(ActionOrdre.ORDRE_REFRESH);
            this.verifFinPartie();
        } catch (CaseNonVideException e) {
            this.fireOrdre(ActionOrdre.ORDRE_CASE_NON_VIDE);
        }
    }

    public Joueur getCurrentPlayer() {
        return this.facadeModele.getCurrentPlayer();
    }

    public Joueur getWinner() {
        return this.facadeModele.getWinner();
    }

    public void finPartie() {
        this.facadeModele.reinitialiser();
        this.fireOrdre(ActionOrdre.ORDRE_REFRESH);
        this.fireOrdre(GOTO_PREPARATION);
    }

    public void ragequit(Joueur currentPlayer) {
        this.facadeModele.ragequit(currentPlayer);
        this.verifFinPartie();
    }
}
