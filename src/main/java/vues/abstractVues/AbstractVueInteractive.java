package vues.abstractVues;

import controleur.ControleurImpl;
import controleur.ordres.GoToOrdre;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import vues.interfaces.VueInteractive;

public abstract class AbstractVueInteractive extends AbstractVue implements VueInteractive {

    private ControleurImpl controleur;


    @Override
    public ControleurImpl getControleur() {
        return controleur;
    }

    @Override
    public void setControleur(ControleurImpl controleur) {
        this.controleur = controleur;
    }

    public void goTo(ActionEvent e){
        this.controleur.goTo(GoToOrdre.valueOf(((String) ((Node) e.getSource()).getUserData()).toUpperCase()));
    }

}
