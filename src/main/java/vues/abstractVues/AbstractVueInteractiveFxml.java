package vues.abstractVues;

import javafx.scene.Parent;

public abstract class AbstractVueInteractiveFxml extends AbstractVueInteractive{

    private Parent parent;

    public abstract String getFXmlFileName();

    @Override
    public Parent getTopParent() {
        return this.parent;
    }

    public void setParent(Parent parent) {
        this.parent = parent;
    }

}
