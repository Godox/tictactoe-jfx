package vues.fxml;

import controleur.ordres.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import modele.Joueur;
import modele.Plateau;
import modele.Symbole;
import vues.abstractVues.AbstractVueInteractiveFxml;

import java.util.Arrays;
import java.util.List;

public class PlateauVue extends AbstractVueInteractiveFxml implements EcouteurOrdre {

    @FXML
    public Button btn0;
    public Button btn1;
    public Button btn2;
    public Button btn3;
    public Button btn4;
    public Button btn5;
    public Button btn6;
    public Button btn7;
    public Button btn8;
    public Label turnLabel;

    private Button[] buttons;

    @Override
    public void afterControlerInitialisation() {
        buttons = new Button[]{btn0, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8};
    }

    public void rafraichirPlateau() {
        List<Symbole> pC = this.getPlateau().cases;
        for (int i = 0; i < pC.size(); i++) {
            this.buttons[i].setText(String.valueOf(pC.get(i).getCharacter()));
        }
    }

    public Plateau getPlateau() {
        return this.getControleur().getPlateau();
    }

    public void jouerCoup(ActionEvent e) {
        Button b = ((Button) e.getSource());
        this.getControleur().jouerCoup(Arrays.asList(this.buttons).indexOf(b));
    }

    @Override
    public String getFXmlFileName() {
        return "plateau.fxml";
    }

    @Override
    public void setAbonnement(LanceurOrdre g) {
        g.abonnement(this,
                ActionOrdre.ORDRE_REFRESH,
                ActionOrdre.ORDRE_PARTIE_TERMINEE,
                ActionOrdre.ORDRE_NOUVEAU_TOUR
        );
    }

    @Override
    public void traiter(Ordre e) {
        switch (((ActionOrdre) e)) {
            case ORDRE_REFRESH -> this.rafraichirPlateau();
            case ORDRE_NOUVEAU_TOUR -> {
                this.alertNouveauTour();
                this.updateNomJoueur();
            }
            case ORDRE_PARTIE_TERMINEE -> this.partieTerminee();
        }
    }

    private void updateNomJoueur() {
        this.turnLabel.setText("Tour de " + getCurrentPlayer().getName());
    }

    private void partieTerminee() {
        Alert a = new Alert(Alert.AlertType.INFORMATION);
        a.setTitle("Partie terminée");
        Joueur winner = this.getWinner();
        if (winner == null){
            a.setContentText("Egalité personne n'a gagné");
        }else {
            a.setContentText("La partie est terminée le gagnant est " + winner.getName());
        }
        a.showAndWait();
        this.getControleur().finPartie();
    }

    private Joueur getWinner() {
        return this.getControleur().getWinner();
    }

    private void alertNouveauTour() {
        Alert a = new Alert(Alert.AlertType.INFORMATION);
        a.setTitle("Nouveau tour");
        a.setContentText("C'est au tour de " + this.getCurrentPlayer().getName());
        a.showAndWait();
    }

    private Joueur getCurrentPlayer() {
        return this.getControleur().getCurrentPlayer();
    }

    public void ragequit(ActionEvent actionEvent) {
        this.getControleur().ragequit(getCurrentPlayer());
    }
}
