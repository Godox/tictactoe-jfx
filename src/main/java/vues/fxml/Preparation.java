package vues.fxml;

import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import vues.abstractVues.AbstractVueInteractive;

public class Preparation extends AbstractVueInteractive {
    private BorderPane borderPane;
    private TextField champJ1;
    private TextField champJ2;
    private Button btnValid;

    @Override
    public Parent getTopParent() {
        return this.borderPane;
    }

    public static Preparation creerVue(){
        Preparation prep = new Preparation();
        prep.buildVue();
        return prep;
    }

    private void buildVue() {
        this.borderPane = new BorderPane();
        this.champJ1 = new TextField();
        this.champJ2 = new TextField();
        this.btnValid = new Button("Lancer la partie");

        this.btnValid.setOnAction(this::startGame);

        this.champJ1.setPromptText("Nom du joueur 1");
        this.champJ2.setPromptText("Nom du joueur 2");

        VBox mainVbox = new VBox();

        Label titre = new Label("Mon super Morpion");
        titre.setFont(new Font(32));

        mainVbox.getChildren().addAll(titre,this.champJ1, this.champJ2, this.btnValid);
        mainVbox.setSpacing(30);

        this.btnValid.setAlignment(Pos.CENTER);
        mainVbox.setAlignment(Pos.CENTER);

        this.champJ1.setMaxWidth(200);
        this.champJ2.setMaxWidth(200);
        this.champJ1.setAlignment(Pos.CENTER);
        this.champJ2.setAlignment(Pos.CENTER);

        this.borderPane.setCenter(mainVbox);

        this.setScene(new Scene(this.borderPane, 600, 400));
    }

    public void startGame(ActionEvent action){
        this.getControleur().lancerPartie(this.champJ1.getText(), this.champJ2.getText());
    }

}
