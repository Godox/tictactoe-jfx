package vues.gestion;

import controleur.ordres.ActionOrdre;
import controleur.ordres.GoToOrdre;
import controleur.ordres.LanceurOrdre;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import vues.abstractVues.AbstractGestionnaireVue;
import vues.fxml.PlateauVue;
import vues.fxml.Preparation;
import vues.interfaces.VueInteractive;


public class GestionnairevueImpl extends AbstractGestionnaireVue {

//    LOAD FXML VIEW EXAMPLE
    private final Preparation preparation = Preparation.creerVue();
    private final PlateauVue plateau = this.loadFxmlView(PlateauVue.class);


    public GestionnairevueImpl(Stage stage) {
        super(stage);
        this.initialize();
        getStage().setScene(this.preparation.getScene());
    }

    @Override
    public void setAbonnementActions(LanceurOrdre g) {
        g.abonnement(this,
                ActionOrdre.ORDRE_CASE_NON_VIDE
        );
    }


    @Override
    public void traiterActions(ActionOrdre e) {
            switch (e){
                case ORDRE_CASE_NON_VIDE: this.alertCaseNonVide();
            }
    }
    private void alertCaseNonVide() {
        Alert a = new Alert(Alert.AlertType.ERROR);
        a.setTitle("Case déjà jouée !");
        a.setContentText("La case que vous avez choisis à déjà été jouée !");
        a.showAndWait();
    }

    @Override
    public void setGotos() {

        GoToOrdre.GOTO_PREPARATION.setGotoView(this.preparation);
        GoToOrdre.GOTO_PLATEAU.setGotoView(this.plateau);

    }

    private void sayHello() {
        Alert al = new Alert(Alert.AlertType.ERROR, "Hello World");
        al.setTitle("Hello");
        al.showAndWait();
    }

}
