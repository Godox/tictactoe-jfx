package vues.interfaces;

import controleur.ControleurImpl;

public interface VueInteractive extends Vue {
    ControleurImpl getControleur();

    void setControleur(ControleurImpl controleur);
}
