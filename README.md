# TD 2 - Tic tac toe

Vous devez développer le modèle du jeu appelé « Tic Tac Toe » aussi connu sous le nom du « Morpion ». Le principe est
simple : sur une grille 3x3 les joueurs tenteront d'aligner trois de leurs pions (verticalement, horizontalement ou en
diagonale) en jouant alternativement. Le premier parvenant à réussir à aligner trois de ses pions sera déclaré
vainqueur.

La facade est déjà implémentée le but est de se servir de celle-ci pour créer une application jfx.

## Use Cases

Le Use case classique

    * Vous arrivez sur un écran avec deux champs texte: le nom du Joueur 1 et le nom du Joueur2 que vous initialisez avec la méthode setJoueurs 
    * Une fois la connexion validée vous arrivez sur le plateau du jeu et c'est à vous de jouer.
    * Vous devez afficher le plateau avec les symboles récuprable en récupérant de plateau de la facade.
    * Ensuite c'est le tour de l'autre joueur (pensez à notifier le nouveau tour de jeu).
    * A chaques coups joués vous pouvez vérifier si celle-ci est terminée avec la méthode estTerminee, et en conséquence jouer un nouveau coup ou terminer la partie.
    * Une fois la facadeTicTacToe terminée, le résultat de la facadeTicTacToe est affiché avant de retourner au menu pour créer la partie afin de pouvoir rejouer.

En facadeTicTacToe, chaque joueur peut "ragequit" et ainsi donner la victoire à l'adversaire. Pour ça il suffit d'appeler la méthode ragequit(joueur)

## Règles de gestion

* Les logins doivent au moins avoir une taille de 3 caractères.
* Les joueurs doivent avoir deux pseudos différents
* Le premier joueur saisi est toujours celui qui commence.
* On ne peut pas jouer en dehors des coordonnées du plateau ((x,y) avec x et y dans {0,1,2}).